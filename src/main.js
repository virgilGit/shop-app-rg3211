// 导入依赖
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

// 配置
Vue.config.productionTip = false
Vue.prototype.$axios=axios

// 核心对象
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

